library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity dec7seg is
  port (
    A : in std_logic ;
    B : in std_logic ;
    C : in std_logic ;
    D : in std_logic ;
    seg : out std_logic_vector (7 downto 0)
  );
end dec7seg;

architecture synthesis of dec7seg is

  -- signal interne pour regrouper les entrées en bus :
  --   facilite les tests : on peut comparer un std_logic_vector avec
  --   un integer
  signal dec : std_logic_vector (3 downto 0);

begin
  dec <= A&B&C&D;
  seg <= "00111111" when dec =  0 else
         "00000110" when dec =  1 else
         "01011011" when dec =  2 else
         "01001111" when dec =  3 else
         "01100110" when dec =  4 else
         "01101101" when dec =  5 else
         "01111101" when dec =  6 else
         "00000111" when dec =  7 else
         "01111111" when dec =  8 else
         "01101111" when dec =  9 else
         "01110111" when dec = 10 else
         "01111100" when dec = 11 else
         "00111001" when dec = 12 else
         "01011110" when dec = 13 else
         "01111001" when dec = 14 else
         "01110001";

end synthesis;
