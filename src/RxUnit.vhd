library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

-- Interface de l'unité de réception
entity RxUnit is
  port (
    clk, reset       : in  std_logic;
    enable           : in  std_logic;
    rd               : in  std_logic;
    rxd              : in  std_logic;
    data             : out std_logic_vector(7 downto 0);
    Ferr, OErr, DRdy : out std_logic);
end RxUnit;

-- Architecture de notre composant de réception
architecture receiver of RxUnit is

  -- Décrit les états possibles du compteur qui nous permet de nous
  -- caler à la demi-période du signal reçu, afin d'être sûr de lire
  -- la bonne donnée.
  type etat_compteur is (init, reception);

  -- Indique au contrôleur de réception quand lire la donnée
  -- ie: passe à 1 lorsqu'on se trouve à la demi-période du
  -- signal reçu.
  signal tmpclk : std_logic := '0';

  -- Image de RxD passée au contrôleur
  signal tmprxd : std_logic := '1';

begin  -- receiver of RxUnit is
  p_compteur : process(enable, reset)

    -- Compte le nombre de fronts montants sur enable
    -- On initialise cette variable à 8, afin de se caler
    -- sur la demi-période. En effet, au premier débordement
    -- du compteur, on sera au milieu du signal et ceci se répétera
    -- périodiquement à chaque débordement.
    variable cpt : std_logic_vector(3 downto 0) := "1000";

     -- Etat du compteur
    variable etat : etat_compteur := init;

  begin
    -- On remet nos différentes variables à leurs valeurs par
    -- defaut.
    if (reset = '0') then
      cpt := "1000";
      tmpclk <= '0';
      tmprxd <= '1';
      etat := init;

    -- Front montant sur enable
    elsif (enable = '1' and enable'event) then
      -- Début de la description de l'automate.
      case etat is
        -- Au départ et après la réception de toute commande de reset,
        -- on se trouve dans l'état init
        when init =>
          cpt := cpt + 1;
          tmpclk <= '0';
          -- Tant qu'il n'y a pas de donnée à lire, on envoie le bit de stop.
          tmprxd <= '1';
          -- Au débordement du compteur, on peut passer à la réception, car on
          -- s'est déphasé d'une demi-période.
          if (cpt = "0000") then
            etat := reception;
          end if;
        -- Dès lors qu'on est dans cet état, on y reste tant qu'on n'a pas
        -- reçu de commande de reset.
        when reception =>
          tmpclk <= '0';
          cpt := cpt + 1;
          -- On copie les données de rxd dans tmprxd afin que l'automate qui
          -- gère le contrôle de réception puisse les lire
          tmprxd <= rxd;
          -- Au débordement de compteur, on indique au contrôleur de réception
          -- que la donnée est prête à être lue.
          if (cpt = "0000") then
            tmpclk <= '1';
          end if;

        when others => null;

      end case;

    end if;

  end process;

  -- purpose: Contrôle la réception des données
  -- type   : combinational
  -- inputs : enable, reset
  -- outputs: data, FErr, OErr, DRdy
  ctrl_reception: process (enable, reset)
    -- Décrit l'état du contrôleur.
    type etat_ctl is (init, lecture, fin_lecture, erreur);

    -- Sous-type du compteur de données.
    subtype t_cpt is integer range 0 to 7;

    -- Variable d'état
    variable etat : etat_ctl := init;

    -- Données en sortie du récepteur
    variable data_out : std_logic_vector(7 downto 0) := (others => '0');

    -- Donne l'indice de la donnée à lire (du bit de poids fort au
    -- bit de poids faible). 
    variable cpt : t_cpt := 7;
  begin  -- process ctrl_reception
    -- Reset de l'unité de contrôle
    if (reset = '0') then
      etat := init;
      data_out := (others => '0');
      cpt := 7;
      Ferr <= '0';
      OErr <= '0';
      DRdy <= '0';
      data <= (others => '0');
    -- Front montant sur enable
    elsif (enable = '1' and enable'event) then
      -- Début de la description de l'automate
      case etat is
        -- Etat initial du contrôleur
        when init =>
          cpt := 7;
          Ferr <= '0';
          OErr <= '0';
          DRdy <= '0';
          -- On réagit sur tmpclk
          if (tmpclk = '1') then
            -- Si la donnée reçue est le bit de stop,
            -- on reste en attente de lecture.
            -- Autrement, la lecture a commencé, donc on passe
            -- en état lecture.
            if (tmprxd = '1') then
              etat := init;
            else
              etat := lecture;
            end if;
          end if;
        -- On lit les données reçues.
        when lecture =>
          -- On réagit sur tmpclk
          if (tmpclk = '1') then
            -- Tant qu'on n'a pas lu les 8 bits de données,
            -- on reste en état de lecture.
            if (cpt >= 0) then
              data_out(cpt) := tmprxd;
              cpt := cpt - 1;
            else
              -- Si on a lu les 8 bits de données, soit on reçoit
              -- le bit de stop et on indique que les données sont
              -- prêtes dans le buffer de sortie. Autrement, on indique
              -- une erreur sur le bit de fin.
              if (tmprxd = '0') then
                Ferr <= '1';
                etat := erreur;
              else
                DRdy <= '1';
                data <= data_out;
                etat := fin_lecture;
              end if;
            end if;
          end if;
        -- On a finit la lecture.
        when fin_lecture =>
          DRdy <= '0';
          -- Soit le processeur nous indique qu'il a bien lu la donnée (rd='1')
          -- et on repasse en état d'initialisation pour lire la prochaine donnée.
          -- Soit il y a eu une erreur de lecture de la part du processeur, et
          -- on l'indique grâce à OErr.
          if (rd = '1') then
            etat := init;
          else
            OErr <= '1';
            etat := erreur; 
          end if;
        -- Cet état n'est pas nécessaire, mais augmente la compréhension de l'automate.
        when erreur =>
          OErr <= '0';
          Ferr <= '0';
          etat := init;

      end case;

    end if;

  end process;
  
end receiver;
