library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity clkUnit is
  
 port (
   clk, reset : in  std_logic;
   enableTX   : out std_logic;
   enableRX   : out std_logic);
    
end clkUnit;


architecture arch_clkUnit of clkUnit is

begin  -- arch_clkUnit

  -- purpose: Génère les signaux d'horloge pour la lecture et l'écriture sur la liaision RS232
  -- type   : combinational
  -- inputs : clk, reset
  -- outputs: enableTX, enableRX
  pclkUnit : process (clk, reset)
    variable cpt : std_logic_vector(3 downto 0) := "0000";  -- Compteur qui donne le nombre de ticks d'horloge effectués
  begin  -- process clkUnit
    if reset = '0' then
      cpt := "0000";
      enableTX <= '0';
    elsif clk = '1' and clk'event then
      if cpt = "1111" then
        enableTX <= '1';
      else
        enableTX <= '0';
      end if;
      cpt := cpt + 1;
    end if;
  end process pclkUnit;

  enableRX <= clk when reset = '1' else '0';


end arch_clkUnit;
