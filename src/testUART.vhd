library IEEE;
use IEEE.std_logic_1164.all;
use work.n7.all;
ENTITY testUART IS
END testUART;
 
ARCHITECTURE behavior OF testUART IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT UARTunit
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         cs : IN  std_logic;
         rd : IN  std_logic;
         wr : IN  std_logic;
         RxD : IN  std_logic;
         TxD : OUT  std_logic;
         IntR : OUT  std_logic;
         IntT : OUT  std_logic;
         addr : IN  std_logic_vector(1 downto 0);
         data_in : IN  std_logic_vector(7 downto 0);
         data_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk1 : std_logic := '0';
   signal reset1 : std_logic := '0';
   signal cs1 : std_logic := '1';
   signal rd1 : std_logic := '0';
   signal wr1 : std_logic := '1';
   signal RxD1 : std_logic := '0';
   signal addr1 : std_logic_vector(1 downto 0) := (others => '0');
   signal data_in1 : std_logic_vector(7 downto 0) := (others => '0');
	
   signal clk2 : std_logic := '0';
   signal reset2 : std_logic := '0';
   signal cs2 : std_logic := '0';
   signal rd2 : std_logic := '0';
   signal wr2 : std_logic := '0';
   signal RxD2 : std_logic := '0';
   signal addr2 : std_logic_vector(1 downto 0) := (others => '0');
   signal data_in2 : std_logic_vector(7 downto 0) := (others => '0');

   --Outputs
   signal TxD1 : std_logic;
   signal IntR1 : std_logic;
   signal IntT1 : std_logic;
   signal data_out1 : std_logic_vector(7 downto 0);
	
   signal TxD2 : std_logic;
   signal IntR2 : std_logic;
   signal IntT2 : std_logic;
   signal data_out2 : std_logic_vector(7 downto 0);

 
BEGIN

    -- 155 Khz = 6.4 micro sec de période
    -- vaar clk, periode, debut, duree
    C1: clock(clk1, 6400 ns, 0 ns, 6400*1000 ns);
    C2: clock(clk2, 400 ns, 0 ns, 6400*1000 ns);
 
    -- Instantiate the Unit Under Test (UUT) 1 =(emits)
    uut1: UARTunit PORT MAP (
	  clk => clk1,
	  reset => reset1,
	  cs => cs1,
	  rd => rd1,
	  wr => wr1,
	  RxD => RxD1,
	  TxD => TxD1,
	  IntR => IntR1,
	  IntT => IntT1,
	  addr => addr1,
	  data_in => data_in1,
	  data_out => data_out1
	);
    
    -- Instantiate the Unit Under Test (UUT) 2 = (receives)
    uut2: UARTunit PORT MAP (
	  clk => clk2,
	  reset => reset2,
	  cs => cs2,
	  rd => rd2,
	  wr => wr2,
	  RxD => RxD2,
	  TxD => TxD2,
	  IntR => IntR2,
	  IntT => IntT2,
	  addr => addr2,
	  data_in => data_in2,
	  data_out => data_out2
	);
		  
    reset1 <= '0', '1' after 75 ns;
    reset2 <= '0', '1' after 75 ns;

    RxD1<=TxD2 after 10 us;
    RxD2<=TxD1 after 10 us;
 

   -- Stimulus process
   stim_proc: process
   begin	
     wait until reset1='1';
     
     data_in1<="00011000";
     
     if IntT1='1' then
	    wait until IntT1='0';
     end if;
     
     -- INT1 = 0 <-- BufE = '1' or RegE = '1'
     -- écriture dans bufferE, ld=1
     cs1 <= '0';
     wr1 <= '0';
     
     if IntT1='0' then
	    wait until IntT1='1';
     end if;
     
     
     wr1 <= '1';
     rd1 <= '1';
		 

   end process;

END;
