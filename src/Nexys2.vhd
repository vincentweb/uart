library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity Nexys2 is
  port (
    -- les 8 switchs
    swt : in std_logic_vector (7 downto 0) ;
    -- les anodes pour slectionner l'afficheur 7 segments
    an : out std_logic_vector (3 downto 0) ;
    -- afficheur 7 segments
    ssg : out std_logic_vector (7 downto 0) ;
    -- horloge
    mclk : in std_logic ;
    -- les 4 boutons
    btn : in std_logic_vector (3 downto 0) ;
    -- les 8 leds
    led : out std_logic_vector (7 downto 0) ;
    -- ligne RS232
    rxd : in std_logic ;
    txd : out std_logic
    );
end Nexys2;

architecture synthesis of Nexys2 is

  component dec7seg
    port (
      A : in std_logic ;
      B : in std_logic ;
      C : in std_logic ;
      D : in std_logic ;
      seg : out std_logic_vector (7 downto 0)
      ) ;
  end component ;
  
  component UART_FPGA_N2
    port (
      mclk : in std_logic ;
      btn : in std_logic ;
      rxd : in std_logic ;
      txd : out std_logic ) ;
  end component;
  
begin

  -- convention afficheur 7 segments 0 => allumé, 1 => éteint
  ssg <= (others => '0');
  -- Afficheurs
  an(3 downto 0) <= "0000";
  -- 8 leds (éclairées juste pour le fun :))
  led(7 downto 0) <= (others => '1') ;
  
  uart : UART_FPGA_N2 port map (mclk, btn(0), rxd, txd);
  
end synthesis;
