library IEEE;
use IEEE.std_logic_1164.all;

-- Interface de l'unité d'émission
entity TxUnit is
  port (
    clk, reset : in 	std_logic;
    enable 		: in 	std_logic;
    ld 			: in 	std_logic;
    txd 			: out std_logic;
    regE 		: out std_logic;
    bufE 		: out std_logic;
    data 		: in 	std_logic_vector(7 downto 0));
end TxUnit;

-- Architecture de notre composant d'émission
-- Dans un premier temps, nous avons tenté d'écrire
-- ce composant en deux process, afin de séparer la gestion
-- des buffers et celle de l'envoi des données.
-- Cependant, les process de gestion des buffers et d'envoi
-- des données n'étant pas actifs sur la même horloge, nous
-- n'avons pas réussi à synchroniser les automates et avons
-- décidé de passer à un modèle décrit par un unique automate.
architecture transmitter of TxUnit is

  -- Décrit les états possibles de notre automate
  type t_etat is (init, buffer_charge, registre_charge, envoi_start, envoi_donnees, envoi_stop);

  -- Signal indiquant si le buffer est plein ou non.
  signal bufferE : std_logic_vector(7 downto 0);

  -- Signal indiquant si le registre est plein ou non.
  signal registerE : std_logic_vector(7 downto 0);

  -- Sous-type permettant de compter le nombre de bits
  -- de données déjà envoyés.
  subtype t_cpt is integer range -1 to 7;

  -- Ce signal est exactement identique à bufferE, à la différence
  -- près qu'on peut y lire et y écrire.
  signal bufE_readable : std_logic;
  
begin

  bufE <= bufE_readable;
  
  ptransmit : process (clk,reset)
    -- Donne l'indice du prochain bit à transmettre.
    variable cpt : t_cpt := 7;
    variable etat : t_etat := init;
  begin
    -- Reset
    if (reset = '0') then
      txd <= '1';
      regE <= '1';
      bufE_readable <= '1';
      etat := init;
      cpt := 7;
    -- Front montant d'horloge.
    elsif clk = '1' and clk'event then

      case etat is
        -- Etat initial.
        when init =>
          -- Si on reçoit une demande de chargement,
          -- on charge les données dans le buffer et
          -- on indique qu'il n'est plus disponible.
          if (ld = '1') then
            bufE_readable <= '0';
            bufferE <= data;
            etat := buffer_charge;
          end if;
        -- La seule façon de revenir à cet état est d'avoir
        -- rendu disponible le registre. Ainsi, on est sûr
        -- de ne pas écraser de données en passant les données
        -- du buffer au registre directement.
        when buffer_charge =>
          registerE <= bufferE;
          regE <= '0';
          bufE_readable <= '1';
          etat := registre_charge;
        -- Dès lors que le registre est chargé, on a deux cas à
        -- vérifer:
        -- Si on reçoit l'autorisation de transmettre (enable=1)
        -- on commence à émettre.
        -- Si on reçoit une demande de chargement et que le buffer est
        -- libre, on le charge.
        when registre_charge =>
          if (enable = '1') then
            -- Bit de start
            txd <= '0';
            -- On reçoit l'autorisation d'émettre, donc on change d'état
            etat := envoi_start;
          end if;
          if (ld = '1' and bufE_readable = '1') then
            bufE_readable <= '0';
            -- On charge le buffer, mais on reste dans cet état
            bufferE <= data;
          end if;
        -- Début d'émission
        when envoi_start =>
          if (enable = '1') then
            -- On envoie le 1er bit de donnée
            txd <= registerE(cpt);
            etat := envoi_donnees;
            cpt := cpt - 1;
          end if;
          -- On charge le buffer s'il est libre
          if (ld = '1' and bufE_readable = '1') then
            bufE_readable <= '0';
            bufferE <= data;
          end if;

        when envoi_donnees =>
          if (enable = '1') then
            -- Il reste des bits à transmettre
            if (cpt >= 0) then
              txd <= registerE(cpt);
              cpt := cpt - 1;
            else
              -- On a finit l'émission, donc on libère le registre
              -- et on envoie le bit de stop
              regE <= '1';
              txd <= '1';
              etat := envoi_stop;
            end if;
          end if;
          -- On charge le buffer s'il est libre
          if (ld = '1' and bufE_readable = '1') then
            bufE_readable <= '0';
            bufferE <= data;
          end if;
        -- On a finit l'émission
        when envoi_stop =>
          -- Le buffer ne contient pas de données, donc on
          -- repasse à l'état init.
          if (bufE_readable = '1') then
            etat := init;
          -- Le buffer contient des données, donc on les
          -- transfère au registre.
          else
            etat := buffer_charge;
          end if;
          -- Inutile a priori, dans la mesure où on passe à init
          -- si le buffer est libre et que le chargement se fera
          -- donc dans cet état.
          --if (ld = '1' and bufE_readable = '1') then
          --  bufE_readable <= '0';
          --  bufferE <= data;
          -- end if;

      end case;

    end if;

  end process;
  
end transmitter;

