library IEEE;
use IEEE.std_logic_1164.all;

-- Composant implanté sur la carte.
entity UART_FPGA_N2 is
  port (
    mclk : in std_logic ;
    btn : in std_logic ;
    rxd : in std_logic ;
    txd : out std_logic ) ;
end UART_FPGA_N2;

architecture UART_FPGA_N2_arch of UART_FPGA_N2 is
  component UARTunit
    port (
      clk, reset : in  std_logic;
      cs, rd, wr : in  std_logic;
      RxD        : in  std_logic;
      TxD        : out std_logic;
      IntR       : out std_logic;        
      IntT       : out std_logic;         
      addr       : in  std_logic_vector(1 downto 0);
      data_in    : in  std_logic_vector(7 downto 0);
      data_out   : out std_logic_vector(7 downto 0));
  end component;

  component diviseurClk is
    port (
      clk, reset : in std_logic ;
      nclk : out std_logic ) ;
  end component ;

  component echoUnit is
    port(
      clk, reset : in std_logic;
      cs, rd, wr : out std_logic;
      IntR : in std_logic;
      IntT : in std_logic;
      addr : out std_logic_vector(1 downto 0);
      data_in : in std_logic_vector(7 downto 0);
      data_out : out std_logic_vector(7 downto 0));
  end component;
  
  signal clk : std_logic;
  signal cs, rd, wr, intR, intT : std_logic;
  signal addr : std_logic_vector(1 downto 0);
  signal d1, d2 : std_logic_vector(7 downto 0);
  
begin
  clkU : diviseurClk
    port map(mclk, not btn, clk);
  
  uartU : UARTunit
    port map(clk, not btn, cs, rd, wr, rxd, txd, intR, intT, addr, d2, d1);
  
  echoU : echoUnit
    port map(clk, not btn, cs, rd, wr, intR, intT, addr, d1, d2);
  
end UART_FPGA_N2_arch;
